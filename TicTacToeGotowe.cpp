#include <iostream>
#define DEPTH_LEVEL 10
struct Move{
    int row, col;
};

char ** initialize_board(int length){
	
	char **temp = new char*[length];
	
	for(int i = 0; i < length; i++){
        temp[i] = new char[length];
    }
    
    for(int i = 0; i < length; i++){
        for(int j = 0; j < length; j++){
            temp[i][j] = '-';
        }
    }
    return temp;
}
	
void display_board(int length, char **board){
	
	for(int i = 0; i < length; i++){
		std::cout << "|";
		for(int j = 0; j < length; j++){
			std::cout << board[i][j];
		}
		std::cout << "|\n";
	}
}

void make_a_move(char player, char **board, int x, int y){
	board[x][y] = player;
}

bool check_if_empty(char **board, int length, int x, int y){

	if(x >= length || y >= length){
		return false;
	}else if(board[x][y] != '-'){
		return false;
	}else{
		return true;
	}


}

int check_if_someone_won(char **board, int length){
	//Przeszukiwanie wierszy
	for(int i = 0; i < length; i++){
		for(int j = 0; j < length -2 ;j++){
			if(board[i][j] == board[i][j+1] &&  board[i][j] == board[i][j+2] && board[i][j] != '-'){
				if(board[i][j] == 'X'){
					return 10;
				}else{
					return -10;
				}
			}
		}
	}
	//Przeszukiwanie kolumn
	for(int i = 0; i < length - 2; i++){
		for(int j = 0; j < length;j++){
			if(board[i][j] == board[i+1][j] &&  board[i][j] == board[i+2][j] && board[i][j] != '-'){
				if(board[i][j] == 'X'){
					return 10;
				}else{
					return -10;
				}
			}
		}
	}
	//Przeszukiwanie diagonali głównych
	for(int i = 0; i < length - 2 ; i++){
		for(int j = 0; j < length - 2; j++){
			if(board[i][j] == board[i+1][j+1] &&  board[i][j] == board[i+2][j+2] && board[i][j] != '-'){
				if(board[i][j] == 'X'){
					return 10;
				}else{
					return -10;
				}
			}
		}
	}
	//Przeszukiwanie diagonali ukośnych
	for(int i = 0; i < length - 2 ;i++){
		for(int j = length; j >= 2 ;j--){
			if(board[i][j] == board[i+1][j-1] &&  board[i][j] == board[i+2][j-2] && board[i][j] != '-'){
				if(board[i][j] == 'X'){
					return 10;
				}else{
					return -10;
				}
			}
		}
	}
	return 0;
}

bool isMovesLeft(char **board, int length){
    for (int i = 0; i<length; i++)
        for (int j = 0; j<length; j++)
            if (board[i][j]=='-')
                return true;
    return false;
}

int minimax(char **board, int depth, bool isMax, int length)
{
	//std::cout << "Depth: " << depth << "\n";
    int score = check_if_someone_won(board,length);
 
    // If Maximizer has won the game return his/her
    // evaluated score
    if (score == 10)
        return score;
 
    // If Minimizer has won the game return his/her
    // evaluated score
    if (score == -10)
        return score;
 
    // If there are no more moves and no winner then
    // it is a tie
    if (isMovesLeft(board,length)==false)
        return 0;
 
    // If this maximizer's move
    if (isMax)
    {
        int best = -1000;
 
        // Traverse all cells
		if(depth < DEPTH_LEVEL){
        for (int i = 0; i<length; i++)
			{
				for (int j = 0; j<length; j++)
				{
					// Check if cell is empty
					if (board[i][j]=='-')
					{
						// Make the move
						board[i][j] = 'X';
	 
						// Call minimax recursively and choose
						// the maximum value
						best = std::max( best,
							minimax(board, depth+1, !isMax,length) );
	 
						// Undo the move
						board[i][j] = '-';
					}
				}
			}
		}
        return best-depth;
 
    // If this minimizer's move
    }else
    {
        int best = 1000;
		if(depth < DEPTH_LEVEL){
			// Traverse all cells
			for (int i = 0; i<length; i++)
			{
				for (int j = 0; j<length; j++)
				{
					// Check if cell is empty
					if (board[i][j]=='-')
					{
						// Make the move
						board[i][j] = 'O';
	 
						// Call minimax recursively and choose
						// the minimum value
						best = std::min(best,
							   minimax(board, depth+1, !isMax,length));
	 
						// Undo the move
						board[i][j] = '-';
					}
				}
			}
		}
        return best+depth;
    }
}
 
// This will return the best possible move for the player
Move findBestMove(char **board, int length)
{
    int bestVal = -1000;
    Move bestMove;
    bestMove.row = -1;
    bestMove.col = -1;
 
    // Traverse all cells, evaluate minimax function for
    // all empty cells. And return the cell with optimal
    // value.
    for (int i = 0; i<length; i++)
    {
        for (int j = 0; j<length; j++)
        {
            // Check if cell is empty
            if (board[i][j]=='-')
            {
                // Make the move
                board[i][j] = 'X';
 
                // compute evaluation function for this
                // move.
                int moveVal = minimax(board, 0, false,length);
				std::cout << "Aktualna komórka: " << i << " " << j << "\n";
 
                // Undo the move
                board[i][j] = '-';
 
                // If the value of the current move is
                // more than the best value, then update
                // best/
                if (moveVal > bestVal)
                {
                    bestMove.row = i;
                    bestMove.col = j;
                    bestVal = moveVal;
                }
            }
        }
    }

    return bestMove;
} 


int main(){
	
	char player = 'O';
	int x = 0; 
	int y = 0;
	int counter = 0;
	int length = 0;
	std::cout << "Podaj rozmiar pola do gry: ";
	std::cin >> length;
	char ** board = initialize_board(length);
	do{
		std::cout << "Ruch gracza: " << player << "\n";
		display_board(length,board);
		if(counter % 2 == 0){
			player = 'O';
			std::cout << "Podaj swoj ruch: ";
			std::cin >> x >> y;
			x--;
			y--;
			
			if(!check_if_empty(board,length,x,y)){
				std::cout << "Wskazano nieprawidlowe pole!\n";
			}else{
				make_a_move(player,board,x,y);
				counter++;
			}
		}else{
			player = 'X';
			Move move = findBestMove(board,length);
			make_a_move(player,board,move.row,move.col);
			counter++;
			
		}
		
	}while(isMovesLeft(board,length) && !check_if_someone_won(board, length));
	display_board(length,board);
	if(check_if_someone_won(board, length) == 0){
	std::cout << "Koniec gry! Remis!";
	}else{
		std::cout << "Koniec gry! Zwyciezyl gracz: " << player;
	}
	 delete board; 
	
	
	
	return 0;
}